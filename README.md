# Shared Preferences

Les SharedPreferences sont des espaces de stockages propres à chaque application Android. 

Avec un système de clé/valeur, vous pourrez persister vos données simplement.


![Screeshot](https://gitlab.com/claudebueno/sharedpreferences/raw/master/screenshots/Screenshot_1528558141.png)

.

