package com.apertoapp.sharedpreferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView showPref = findViewById(R.id.showSavedPref);
        final EditText txtFillPref = findViewById(R.id.txtFillPref);
        Button btnSave = findViewById(R.id.btnSave);

        final SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String oldItem = sharedPref.getString("oldItem", "Shared preferences est completement vide...");

        showPref.setText("Bonjour " + oldItem);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("oldItem", txtFillPref.getText().toString());
                editor.commit();

                Toast.makeText(MainActivity.this,"Donnée sauvegardée dans Shared preferences", Toast.LENGTH_LONG).show();

                // Refresh current activity
                Intent intent = getIntent();
                finish();
                overridePendingTransition(0, 0);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });


    }
}
